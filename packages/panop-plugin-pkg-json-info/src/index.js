/* eslint-disable class-methods-use-this */
/* eslint-disable no-param-reassign */
/* eslint-disable no-prototype-builtins */

module.exports = class PkgJsonInfoPlugin {

  // Must return unique non-empty string
  get resultKey() { return 'pkgJsonInfo'; }

  // Must return promise
  getData(scopedPackageName, packageDirName, tag, bb) {
    return bb.getFileContents({
      tag,
      path: `packages/${packageDirName}/package.json`,
    });
  }

};
