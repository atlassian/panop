// @flow weak

import PromiseThrottle from 'promise-throttle';
import request from 'request';

function reqPromiseFactory(reqOpts, reqFn) {
  return () => (
    new Promise((resolve, reject) => {
      try {
        reqFn(reqOpts, (err, response, body) => {
          if (err) {
            reject(err);
            return;
          }

          resolve({ response, body });
        });
      } catch (e) {
        reject(e);
      }
    })
  );
}

class RequestThrottle {
  requestsPerSecond: number;
  queue: Object;

  constructor(requestsPerSecond = 20) {
    this.requestsPerSecond = requestsPerSecond;
    this.queue = new PromiseThrottle({
      requestsPerSecond,
      promiseImplementation: Promise,
    });
  }

  // returns a promise which is eventually has the request result
  add(reqOpts, reqFn = request) {
    return this.queue.add(reqPromiseFactory(reqOpts, reqFn));
  }
}

export default RequestThrottle;
