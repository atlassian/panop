// @flow weak

import fs from 'fs';
import compareVersions from 'compare-versions';

export function sortSemverDescending(a, b) {
  return compareVersions(b.version, a.version);
}

export function ensureDirs(requiredDirs) {
  requiredDirs.forEach((dir) => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
  });
}

export function writeFiles(files) {
  files.forEach((file) => {
    fs.writeFileSync(
      file.name,
      file.name.match(/\.json$/) ?
        JSON.stringify(file.content, null, 4) :
        file.content,
      'utf8'
    );
  });
}
