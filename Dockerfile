FROM node:6
RUN apt-get update
RUN apt-get -y install ocaml libelf-dev
RUN npm install -g yarn@latest
